# Use an official Node.js image as the base
FROM node:14

# Set up environment variables for Android SDK
ENV ANDROID_HOME /opt/android-sdk
ENV PATH $PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$ANDROID_HOME/tools/bin

# Install required dependencies
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    wget \
    unzip \
    openjdk-8-jdk \
    && rm -rf /var/lib/apt/lists/*

# Download and install Android SDK
RUN mkdir -p $ANDROID_HOME && \
    cd $ANDROID_HOME && \
    wget -q https://dl.google.com/android/repository/commandlinetools-linux-7302050_latest.zip -O sdk-tools.zip && \
    unzip sdk-tools.zip && \
    rm sdk-tools.zip && \
    yes | sdkmanager --licenses

# Set up environment variables for React Native
ENV REACT_NATIVE_VERSION latest

# Install React Native CLI globally
RUN npm install -g react-native-cli

# Set up working directory
WORKDIR /app

# Copy package.json and package-lock.json if you have one
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the entire project
COPY . .

# Start the build
CMD ["npm", "run", "android"]
