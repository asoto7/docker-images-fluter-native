# Use an official Flutter image as the base
FROM cirrusci/flutter:stable

# Install Android SDK dependencies
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    unzip \
    openjdk-8-jdk \
    && rm -rf /var/lib/apt/lists/*

# Set up Android SDK
ENV ANDROID_SDK_ROOT /opt/android-sdk
RUN mkdir -p ${ANDROID_SDK_ROOT} && \
    cd ${ANDROID_SDK_ROOT} && \
    wget -q https://dl.google.com/android/repository/commandlinetools-linux-6858069_latest.zip -O android-sdk-tools.zip && \
    unzip -q android-sdk-tools.zip && \
    rm android-sdk-tools.zip

ENV PATH ${PATH}:${ANDROID_SDK_ROOT}/cmdline-tools/tools/bin:${ANDROID_SDK_ROOT}/platform-tools:${ANDROID_SDK_ROOT}/emulator:${ANDROID_SDK_ROOT}/tools/bin:${ANDROID_SDK_ROOT}/tools:${ANDROID_SDK_ROOT}/build-tools

# Accept licenses
RUN yes | sdkmanager --licenses

# Install Android SDK components
RUN sdkmanager --update && \
    sdkmanager "platform-tools" "platforms;android-30" "build-tools;30.0.3"

# Set up Flutter
ENV FLUTTER_HOME /opt/flutter
ENV PATH ${PATH}:${FLUTTER_HOME}/bin

# Run basic Flutter doctor commands to pre-cache Flutter artifacts
RUN flutter doctor

# Optionally, you can run 'flutter config --enable-web' if you want to enable web support

# Set the working directory
WORKDIR /app

# Copy the project files into the container
COPY . .

# Run your build commands here
# For example:
# RUN flutter build apk --release

# Entrypoint command (if needed)
CMD ["flutter", "build", "apk", "--release"]
